python manage.py migrate
python manage.py runserver

Onda pokrenemo migracije 
python manage.py makemigrations todo

Onda te migracije apliciramo na bazu 
python manage.py migrate todo

CORS_ORIGIN_WHITELIST = [
     'http://localhost:3000'
]

backend/serializer.py
from rest_framework import serializers
from .models import Todo

class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ('id', 'title', 'description', 'completed')

