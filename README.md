#  Struktura

**Koišten je TokenAuthorization tako da za sve rute osim 1 i 3 treba header oblika -> Authorization Token `<token>`**

Token se može nabaviti na ruti 3 nakon što se prijavio novi korisnik

Korisnici mogu vidjeti, uređivati i brisati samo one todojeve koje su oni napravili.

TODO: nisam znao kak sad: token se može vratiti i odmah kad se novi korisnik registrira
##  Rute
###  Api Auth  

1. `POST /api-auth/register`
	- Potrebni podatci

		    username: [string]
		    password: [string]

2. `GET /api-auth/logout`
3. `POST /api-auth/get-token`

	- Potrebni podatci

		    username: [string]
		    password: [string]

### Todos
1. `GET /api/todo`
2. `GET /api/todo/<id>/`
3. `POST /api/todo`		

    - Napravi novi todo
    - Potrebni podatci

            title: [string]
            description: [string]
    
4. `PUT /api/todo/<id>/`

    - Updejtaj postojeci todo
    - Potrebni podatci

            title: [str]
            description: [str]

5. `DELETE /api/todo/<id>`