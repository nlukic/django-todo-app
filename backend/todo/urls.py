from django.urls import path
from .views import TodoView, TodoDetialsView

urlpatterns = [
    path('todo/', TodoView.as_view(), name='todo'),
    path('todo/<int:id>/', TodoDetialsView.as_view(), name='todo-details')
]
