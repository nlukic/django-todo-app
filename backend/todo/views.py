from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED,
                                   HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_401_UNAUTHORIZED)
from rest_framework.views import APIView

from .models import Todo
from .serializers import TodoSerializer
from .permissions import IsOwner


# todo/
class TodoView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request: Request) -> Response:
        todos = Todo.objects.filter(owner=request.user.id)
        serializer = TodoSerializer(todos, many=True)
        return Response(serializer.data, status=HTTP_200_OK)

    def post(self, request: Request) -> Response:
        serializer = TodoSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(owner=request.user)
            return Response(serializer.data, status=HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


# todo/<id>
class TodoDetialsView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated & IsOwner]

    def get_todo(self, id, req):
        try:
            todo = Todo.objects.get(id=id)
            self.check_object_permissions(req, todo)
            return todo
        except Todo.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

    def get(self, request: Request, id) -> Response:
        todo = self.get_todo(id, request)
        serializer = TodoSerializer(todo)
        return Response(serializer.data, status=HTTP_200_OK)

    def put(self, request: Request, id) -> Response:
        todo = self.get_todo(id, request)
        serializer = TodoSerializer(todo, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        else:
            return Response(serializer.errors, HTTP_400_BAD_REQUEST)

    def delete(self, request: Request, id) -> Response:
        todo = self.get_todo(id, request)
        serializer = TodoSerializer(todo)
        todo.delete()
        return Response(serializer.data, status=HTTP_200_OK)
