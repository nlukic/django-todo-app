from django.urls import path
from .views import RegistrationView, LogoutView
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('register', RegistrationView.as_view(), name='register'),
    path('get-token', obtain_auth_token, name='get-token'),
    path('logout', LogoutView.as_view(), name='logout')
]
